import { BombService } from './bombs/bomb.service';
import { bombConfiguration } from './bombs/bomb.configuration';
import { Component } from '@angular/core';
import { GameClockService } from './shared/game-clock.service';
import { take } from 'rxjs/operators/take';
import { BombState } from './bombs/bomb/bomb.model';

enum GAME_PHASES {
  'START',
  'PLAYING',
  'END'
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  public phase: GAME_PHASES = GAME_PHASES.START;
  public points = 0;

  private _state = {
    initialized: false,
    running: false,
    start: () => {
      this._state.initialized = true;
      this._state.running = true;
      this.phase = GAME_PHASES.PLAYING;
    },
    end: () => {
      this._state.running = false;
      this.phase = GAME_PHASES.END;
    }
  };

  constructor(
    private _gameClockService: GameClockService,
    private _bombService: BombService
  ) { }

  startGame() {
    this.points = 0;
    this._gameClockService.start();
    this._bombService.bombsChange$.pipe(
      take(bombConfiguration.total())
    ).subscribe({
      next: bomb => {
        switch (bomb.getStatus()) {
          case BombState.disarmed:
            this.points += 1;
            break;
          case BombState.exploded:
            this.points -= 1;
            break;
        }
      },
      complete: () => this.endGame()
    });
    this._state.start();
  }

  endGame() {
    this._gameClockService.stop();
    this._state.end();
  }

}
