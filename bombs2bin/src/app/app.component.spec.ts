import { TestBed, async } from "@angular/core/testing";
import { AppComponent } from "./app.component";
import { NO_ERRORS_SCHEMA } from "@angular/core";
import { GameClockService } from "./shared/game-clock.service";
import { BombModel } from "./bombs/bomb/bomb.model";
import { Subject } from "rxjs/Subject";
import { BombService } from "./bombs/bomb.service";

describe("AppComponent", () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AppComponent],
      providers: [
        {
          provide: GameClockService,
          useValue: {
            start: () => {},
            stop: () => {}
          }
        },
        {
          provide: BombService,
          useValue: {
            bombsChange$: new Subject<BombModel>()
          }
        }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  it("should create the app", async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));

  it(`should start at phase 0`, async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.phase).toEqual(0);
  }));

  it("should change to phase 1 when call method startGame", async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    app.startGame();
    expect(app.phase).toEqual(1);
  }));

  it("should change to phase 2 when call method endGame", async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    app.startGame();
    app.endGame();
    expect(app.phase).toEqual(2);
  }));
});
