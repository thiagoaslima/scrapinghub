import { TestBed, inject, tick, fakeAsync } from '@angular/core/testing';

import { GameClockService } from './game-clock.service';

describe('GameClockService', () => {
  let service: GameClockService;

  beforeEach(() => {
    service = new GameClockService();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should start the timer on start method', fakeAsync(() => {
    let timer: number;
    service.clock$.subscribe(value => timer = value);

    service.start();
    expect(timer).toBe(0);

    tick(500);
    expect(timer).toBe(0.5);

    tick(500);
    expect(timer).toBe(1);

    service.stop();
  }));

  it('should stop the timer on stop method', fakeAsync(() => {
    let timer: number;
    service.clock$.subscribe(value => timer = value);

    service.start();
    expect(timer).toBe(0);

    tick(500);
    expect(timer).toBe(0.5);

    service.stop();

    tick(500);
    expect(timer).toBe(0.5);
  }));

  it('should restart on start method', fakeAsync(() => {
    let timer: number;
    service.clock$.subscribe(value => timer = value);

    service.start();
    expect(timer).toBe(0);

    tick(500);
    expect(timer).toBe(0.5);

    service.stop();

    tick(500);
    expect(timer).toBe(0.5);

    service.start();
    expect(timer).toBe(0);

    tick(500);
    expect(timer).toBe(0.5);

    service.stop();
  }));




});
