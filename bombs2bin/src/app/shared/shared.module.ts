import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { DndModule } from "ng2-dnd";
import { ColorsService } from "./colors.service";
import { GameClockService } from "./game-clock.service";
import { bombConfiguration } from "../bombs/bomb.configuration";

@NgModule({
  imports: [CommonModule, DndModule],
  declarations: [],
  providers: [ColorsService, GameClockService],
  exports: [DndModule]
})
export class SharedModule {}
