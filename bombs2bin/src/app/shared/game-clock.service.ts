import { Injectable } from "@angular/core";
import { Subject } from "rxjs/Subject";
import { Subscription } from "rxjs/Subscription";
import { Observable } from "rxjs/Observable";
import { startWith } from "rxjs/operators/startWith";
import "rxjs/add/observable/timer";
import { tap } from "rxjs/operators/tap";
import { take } from "rxjs/operators/take";

@Injectable()
export class GameClockService {
  public clock$ = new Subject<number>();
  private _clockSub: Subscription;

  constructor() {}

  start() {
    this._clockSub = Observable.timer(0, 250)
      .pipe(startWith(0))
      .subscribe(value => this.clock$.next(value / 4));
  }

  stop() {
    this._clockSub.unsubscribe();
  }
}
