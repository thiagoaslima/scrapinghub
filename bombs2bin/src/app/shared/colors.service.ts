import { Injectable } from "@angular/core";
import { getRandomNumber } from "../../utils/randomNumber";

export enum COLORS {
    RED = "#FF4136",
    GREEN = "#3D9970",
    BLUE = "#0074D9"
};

@Injectable()
export class ColorsService {
    getRandomColor() {
        const random = getRandomNumber(1, 3);

        switch (random) {
            case 1:
                return COLORS.RED;
            case 2:
                return COLORS.GREEN;
            case 3:
                return COLORS.BLUE;
        }
    }

    shuffleColors() {
        const cases = [
            [COLORS.RED, COLORS.GREEN, COLORS.BLUE],
            [COLORS.RED, COLORS.BLUE, COLORS.GREEN],
            [COLORS.GREEN, COLORS.RED, COLORS.BLUE],
            [COLORS.GREEN, COLORS.BLUE, COLORS.RED],
            [COLORS.BLUE, COLORS.RED, COLORS.GREEN],
            [COLORS.BLUE, COLORS.GREEN, COLORS.RED]
        ]
        return cases[getRandomNumber(0, 5)];
    }
}