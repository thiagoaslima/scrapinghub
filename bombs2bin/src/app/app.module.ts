import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { DndModule } from "ng2-dnd";

import { AppComponent } from "./app.component";
import { SharedModule } from "./shared/shared.module";
import { BombModule } from "./bombs/bomb.module";
import { BinModule } from "./bins/bin.module";

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    SharedModule,
    DndModule.forRoot(),
    BinModule,
    BombModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
