import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BombComponent } from './bomb/bomb.component';
import { BombService } from './bomb.service';
import { BombsAreaComponent } from './bombs-area/bombs-area.component';
import { bombConfiguration } from './bomb.configuration';
import { GameClockService } from '../shared/game-clock.service';
import { ColorsService } from '../shared/colors.service';
import { SharedModule } from '../shared/shared.module';

export function bombServiceFactory(gameClockService: GameClockService, colorsService: ColorsService) {
  const timeMarkers = bombConfiguration.distribution.reduce((acc, item) => {
    for (let i = 0; i < item.qty; i++) {
      acc.current += item.interval;
      acc.markers.add(acc.current);
    }
    return acc;
  }, { markers: new Set<number>(), current: 0 });

  return new BombService(timeMarkers.markers, gameClockService, colorsService);
}

@NgModule({
  imports: [
    CommonModule,
    SharedModule
  ],
  providers: [
    {
      provide: BombService,
      deps: [GameClockService, ColorsService],
      useFactory: bombServiceFactory
    }
  ],
  declarations: [
    BombComponent,
    BombsAreaComponent
  ],
  exports: [
    BombsAreaComponent
  ]
})
export class BombModule { }
