export const bombConfiguration = {
  distribution: [
    {
      interval: 0,
      qty: 1
    },
    {
      interval: 5,
      qty: 2
    },
    {
      interval: 3.5,
      qty: 5
    },
    {
      interval: 2,
      qty: 8
    },
    {
      interval: 1.5,
      qty: 7
    },
    {
      interval: 1.25,
      qty: 8
    },
    {
      interval: 1,
      qty: 15
    },
    {
      interval: 0.75,
      qty: 16
    },
    {
      interval: 0.5,
      qty: 58
    }
  ],

  total: () =>
    bombConfiguration.distribution.reduce((sum, obj) => sum + obj.qty, 0)
};
