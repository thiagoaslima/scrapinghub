import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs/BehaviorSubject";

import { Observable } from "rxjs/Observable";
import { filter } from "rxjs/operators/filter";
import { map } from "rxjs/operators/map";

import { BombModel } from "./bomb/bomb.model";
import { getRandomNumber } from "../../utils/randomNumber";
import { ColorsService } from "../shared/colors.service";
import { GameClockService } from "../shared/game-clock.service";
import { Subject } from "rxjs/Subject";

@Injectable()
export class BombService {
  private _counter = 0;
  public bombs$: Observable<BombModel>;
  public bombsChange$ = new Subject<BombModel>();
  public clock$: Observable<number>;

  constructor(
    private _timeMarkers: Set<number>,
    private _gameClockService: GameClockService,
    private _colorsService: ColorsService
  ) {
    this.bombs$ = this._gameClockService.clock$.pipe(
      filter(value => this._timeMarkers.has(value)),
      map(value => this.createBomb(value))
    );

  }

  createBomb(time: number) {
    const self = this;
    const lifetime = getRandomNumber(5, 10);

    const bomb = new BombModel({
      id: ++this._counter,
      color: this._colorsService.getRandomColor(),
      totalLifetime: lifetime
    });

    const _disarm = bomb.disarm.bind(bomb);
    bomb.disarm = () => {
      _disarm();
      this.bombsChange$.next(bomb);
    };

    const _explode = bomb.explode.bind(bomb);
    bomb.explode = () => {
      _explode();
      this.bombsChange$.next(bomb);
    };

    return bomb;
  }
}
