import {
  async,
  ComponentFixture,
  TestBed,
  tick,
  fakeAsync
} from "@angular/core/testing";
import { SimpleChange, Component } from "@angular/core";
import { By } from "@angular/platform-browser";
import { DndModule } from "ng2-dnd";

import { BombComponent } from "./bomb.component";
import { COLORS } from "../../shared/colors.service";
import { BombModel } from "./bomb.model";

import { Observable } from "rxjs/Observable";
import "rxjs/add/observable/timer";
import { startWith } from "rxjs/operators/startWith";
import { scan } from "rxjs/operators/scan";
import { Subject } from "rxjs/Subject";

@Component({
  template: `
  <game-bomb [timeLeft]="bomb.timeLeft" [color]="bomb.color"></game-bomb>
  `
})
class TestHostComponent {
  bomb = new BombModel({ id: 42, color: "red", totalLifetime: 17 });
}

describe("BombComponent", () => {
  let hostComponent: TestHostComponent;
  let bombElement: HTMLElement;
  let fixture: ComponentFixture<TestHostComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TestHostComponent, BombComponent],
      imports: [DndModule.forRoot()]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestHostComponent);
    hostComponent = fixture.componentInstance;
    bombElement = fixture.nativeElement.querySelector(".bomb_item");
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(hostComponent).toBeTruthy();
  });

  it(
    "should correctly render the @Input value",
    fakeAsync(() => {
      // starts with the hostComponent bomb
      expect(bombElement.textContent.trim()).toBe("17");
      expect(bombElement.style.backgroundColor).toBe("red");

      // updates current time
      tick(1000);
      fixture.detectChanges();

      fixture.whenStable().then(() => {
        expect(bombElement.textContent.trim()).toBe("16");
        expect(bombElement.style.backgroundColor).toBe("red");

        // remove element
        fixture.destroy();
      });
    })
  );
});
