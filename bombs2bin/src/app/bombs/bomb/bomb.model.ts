import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/observable/interval';
import { take } from 'rxjs/operators/take';

export enum BombState {
  active,
  exploded,
  disarmed
}

export class BombModel {
  public readonly id: number;
  public readonly color: string;
  public readonly totalLifetime: number;
  public timeLeft: number;

  private _state: BombState;
  private _subscription: Subscription;

  constructor({
    id,
    color,
    totalLifetime
  }: {
    id: number;
    color: string;
    totalLifetime: number;
  }) {
    this.id = id;
    this.color = color;
    this.totalLifetime = totalLifetime;
    this.timeLeft = totalLifetime;
    this._activate();
  }

  reduceLifetime() {
    if (this.timeLeft > 0) {
      this.timeLeft -= 1;
    }

    if (this.timeLeft === 0) {
      this.explode();
    }
  }

  getStatus() {
    return this._state;
  }

  explode() {
    this._stop();
    this._state = BombState.exploded;
  }

  disarm() {
    this._stop();
    this._state = BombState.disarmed;
  }

  private _activate() {
    this._createTimer();
    this._state = BombState.active;
  }

  private _stop() {
    this._subscription.unsubscribe();
  }

  private _createTimer() {
    this._subscription = Observable.interval(1000).pipe(
      take(this.totalLifetime)
    ).subscribe(_ => {
      this.reduceLifetime();
    });
  }
}
