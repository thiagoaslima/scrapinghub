import { Component, ChangeDetectionStrategy, Input } from "@angular/core";

import { BombModel } from "./bomb.model";

@Component({
  selector: "game-bomb",
  templateUrl: "./bomb.component.html",
  styleUrls: ["./bomb.component.css"]
})
export class BombComponent {
  @Input() bomb: BombModel;
  @Input() timeLeft: number;
  @Input() color: string;
}
