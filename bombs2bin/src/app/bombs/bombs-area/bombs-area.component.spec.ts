import { async, ComponentFixture, TestBed } from "@angular/core/testing";
import { Component, Input, EventEmitter, Output } from "@angular/core";
import { By } from "@angular/platform-browser";

import { BombsAreaComponent } from "./bombs-area.component";
import { BombService } from "../bomb.service";
import { BombModel } from "../bomb/bomb.model";
import { Subject } from "rxjs/Subject";

@Component({ selector: "game-bomb", template: "" })
class BombStubComponent {
  @Input() bomb: BombModel;
  @Input() timeLeft: number;
  @Input() color: string;
}
describe("BombsAreaComponent", () => {
  let component: BombsAreaComponent;
  let fixture: ComponentFixture<BombsAreaComponent>;
  let bombServiceStub: Partial<BombService>;
  let bombService: BombService;

  beforeEach(async(() => {
    bombServiceStub = {
      bombs$: new Subject<BombModel>(),
      clock$: new Subject<number>(),
      bombsChange$: new Subject<BombModel>()
    };

    TestBed.configureTestingModule({
      declarations: [BombsAreaComponent, BombStubComponent],
      providers: [{ provide: BombService, useValue: bombServiceStub }]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BombsAreaComponent);
    component = fixture.componentInstance;
    bombService = fixture.debugElement.injector.get(BombService);
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });

  it("should create a BombComponent when bombs$ triggers", () => {
    const bomb = new BombModel({
      id: 1,
      color: "red",
      totalLifetime: 1
    });

    let items = fixture.debugElement.queryAll(By.css("game-bomb"));
    expect(items.length).toBe(0);

    (<Subject<BombModel>>bombService.bombs$).next(bomb);
    fixture.detectChanges();

    items = fixture.debugElement.queryAll(By.css("game-bomb"));
    expect(items.length).toBe(1);
  });

  it("should remove a BombComponent when bombsChange$ triggers", () => {
    const bomb = new BombModel({
      id: 1,
      color: "red",
      totalLifetime: 1
    });

    let items = fixture.debugElement.queryAll(By.css("game-bomb"));
    expect(items.length).toBe(0);

    (<Subject<BombModel>>bombService.bombs$).next(bomb);
    fixture.detectChanges();

    items = fixture.debugElement.queryAll(By.css("game-bomb"));
    expect(items.length).toBe(1);

    bombService.bombsChange$.next(bomb);
    fixture.detectChanges();

    items = fixture.debugElement.queryAll(By.css("game-bomb"));
    expect(items.length).toBe(0);
  });
});
