import { Component, OnInit, OnDestroy } from "@angular/core";

import { BombModel } from "../bomb/bomb.model";
import { BombService } from "../bomb.service";

import { Subscription } from "rxjs/Subscription";
import { Observable } from "rxjs/Observable";
import { getRandomNumber } from "../../../utils/randomNumber";

@Component({
  selector: "game-bombs-area",
  templateUrl: "./bombs-area.component.html",
  styleUrls: ["./bombs-area.component.css"]
})
export class BombsAreaComponent implements OnInit, OnDestroy {
  public bombs: BombModel[] = [];
  public positions: { column: number; line: number }[] = [];

  private _subscriptions: Subscription[] = [];

  constructor(private _bombService: BombService) {}

  ngOnInit() {
    const bombSub = this._bombService.bombs$.subscribe(bomb => {
      let position;

      do {
        position = this.drawPosition();
      } while (this.isPositionUsed(position));

      this.bombs.push(bomb);
      this.positions.push(position);
    });

    const bombsChange = this._bombService.bombsChange$.subscribe(bomb =>
      this.removeBomb(bomb)
    );

    this._subscriptions.push(bombSub);
  }

  removeBomb(bomb: BombModel) {
    const idx = this.bombs.indexOf(bomb);
    this.bombs.splice(idx, 1);
    this.positions.splice(idx, 1);
  }

  ngOnDestroy() {
    this._subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  private drawPosition() {
    const line = getRandomNumber(1, 4);
    let column;

    if (line % 2 === 0) {
      column = getRandomNumber(1, 8);
    } else {
      column = getRandomNumber(1, 9);
    }

    return { column, line };
  }

  private isPositionUsed(position: { column: number; line: number }) {
    const idx = this.positions.findIndex(
      pos => pos.column === position.column && pos.line === position.line
    );
    return idx > -1;
  }
}
