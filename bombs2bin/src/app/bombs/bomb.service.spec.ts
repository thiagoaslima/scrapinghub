import { TestBed, inject, fakeAsync, tick } from "@angular/core/testing";

import { BombService } from "./bomb.service";
import { GameClockService } from "../shared/game-clock.service";
import { Subject } from "rxjs/Subject";
import { ColorsService, COLORS } from "../shared/colors.service";
import { BombModel } from "./bomb/bomb.model";

describe("BombService", () => {
  let service: BombService;

  const timeMarkers = new Set<number>([0, 4, 10]);

  const gameClockServiceStub: Partial<GameClockService> = {
    clock$: new Subject<number>()
  };

  const colorsServiceStub: Partial<ColorsService> = {
    getRandomColor: () => COLORS.RED
  };

  beforeEach(() => {
    service = new BombService(
      timeMarkers,
      gameClockServiceStub as GameClockService,
      colorsServiceStub as ColorsService
    );
  });

  it("should be created", () => {
    expect(service).toBeTruthy();
  });

  it("should only create bombs on timeMarkers values", () => {
    let currBomb;
    service.bombs$.subscribe(value => (currBomb = value));

    gameClockServiceStub.clock$.next(0);
    expect(currBomb.id).toBe(1);

    gameClockServiceStub.clock$.next(1);
    expect(currBomb.id).toBe(1);

    gameClockServiceStub.clock$.next(4);
    expect(currBomb.id).toBe(2);

    gameClockServiceStub.clock$.next(8.5);
    expect(currBomb.id).toBe(2);

    gameClockServiceStub.clock$.next(10);
    expect(currBomb.id).toBe(3);
  });
});
