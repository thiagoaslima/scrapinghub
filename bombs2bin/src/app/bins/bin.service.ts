import { Injectable } from '@angular/core';

import { ColorsService } from '../shared/colors.service';
import { GameClockService } from '../shared/game-clock.service';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/interval';
import { filter } from 'rxjs/operators/filter';
import { map } from 'rxjs/operators/map';
import { tap } from 'rxjs/operators/tap';
import { startWith } from 'rxjs/operators/startWith';

@Injectable()
export class BinService {
  public remainingTime$: Observable<number>;
  public colors$: Observable<string[]>;

  constructor(
    private _timeThreshold: number,
    private _gameClock: GameClockService,
    private _colorsService: ColorsService
  ) {

    const bySecond$ = this._gameClock.clock$.pipe(
      filter(value => Number.isInteger(value))
    )

    this.remainingTime$ = bySecond$.pipe(
      map(value => this._calculateClockTime(value))
    );

    this.colors$ = this.remainingTime$.pipe(
      filter(value => value === this._timeThreshold),
      map(_ => this._colorsService.shuffleColors())
    );
  }

  private _calculateClockTime(value) {
    return this._timeThreshold - (value % this._timeThreshold)
  }

}
