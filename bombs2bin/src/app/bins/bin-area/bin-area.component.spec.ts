import { async, ComponentFixture, TestBed } from "@angular/core/testing";
import { Component, Input } from "@angular/core";

import { BinAreaComponent } from "./bin-area.component";
import { BinService } from "../bin.service";

@Component({ selector: "game-bin-droppable", template: "" })
class BinDroppableStubComponent {
  @Input() color: string;
}
@Component({ selector: "game-bin-clock", template: "" })
class BinClockStubComponent {
  @Input() time: number;
}
describe("BinAreaComponent", () => {
  let component: BinAreaComponent;
  let fixture: ComponentFixture<BinAreaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        BinAreaComponent,
        BinClockStubComponent,
        BinDroppableStubComponent
      ],
      providers: [{ provide: BinService, useValue: {} }]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BinAreaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
