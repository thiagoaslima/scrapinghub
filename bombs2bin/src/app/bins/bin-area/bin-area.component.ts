import { Component, OnInit } from '@angular/core';

import { BinService } from '../bin.service';

import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'game-bin-area',
  templateUrl: './bin-area.component.html',
  styleUrls: ['./bin-area.component.css']
})
export class BinAreaComponent implements OnInit {
  public colors: Observable<string[]>;
  public clock: Observable<number>

  constructor(
    private _binService: BinService
  ) { }

  ngOnInit() {
    this.clock = this._binService.remainingTime$;
    this.colors = this._binService.colors$;
  }

}
