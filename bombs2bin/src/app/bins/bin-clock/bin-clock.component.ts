import { Component, Input } from '@angular/core';
import { BinService } from '../bin.service';

@Component({
  selector: 'game-bin-clock',
  templateUrl: './bin-clock.component.html',
  styleUrls: ['./bin-clock.component.css']
})
export class BinClockComponent {
  @Input() time: number

  constructor() { }

}
