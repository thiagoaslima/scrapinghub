import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BinClockComponent } from './bin-clock.component';

describe('BinClockComponent', () => {
  let component: BinClockComponent;
  let fixture: ComponentFixture<BinClockComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BinClockComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BinClockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
