import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BinDroppableComponent } from './bin-droppable.component';

describe('BinDroppableComponent', () => {
  let component: BinDroppableComponent;
  let fixture: ComponentFixture<BinDroppableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BinDroppableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BinDroppableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
