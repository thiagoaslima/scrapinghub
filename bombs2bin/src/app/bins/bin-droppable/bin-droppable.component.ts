import { Component, Input } from "@angular/core";
import { BombModel, BombState } from "../../bombs/bomb/bomb.model";

@Component({
  selector: "game-bin-droppable",
  templateUrl: "./bin-droppable.component.html",
  styleUrls: ["./bin-droppable.component.css"]
})
export class BinDroppableComponent {
  @Input() color: string;
  isDropHover = false;

  constructor() {}

  handleDrag({
    mouseEvent,
    dragData
  }: {
    mouseEvent: DragEvent;
    dragData: BombModel;
  }) {
    switch (mouseEvent.type) {
      case "dragenter":
        this.isDropHover = this.color === dragData.color;
        break;
      default:
        this.isDropHover = false;
    }
  }

  handleDrop({
    mouseEvent,
    dragData
  }: {
    mouseEvent: DragEvent;
    dragData: BombModel;
  }) {
    this.isDropHover = false;

    if (dragData.getStatus() !== BombState.active) {
      return false;
    }

    if (dragData.color === this.color) {
      dragData.disarm();
    } else {
      dragData.explode();
    }
  }
}
