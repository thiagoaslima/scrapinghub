import { fakeAsync, tick } from '@angular/core/testing';

import { BinService } from './bin.service';
import { GameClockService } from '../shared/game-clock.service';
import { ColorsService } from '../shared/colors.service';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/timer';
import { map } from 'rxjs/operators/map';

describe('BinService', () => {
  const timeThreshold = 5;
  let service: BinService;
  let fakeClockService;
  let fakeColorsService: jasmine.SpyObj<ColorsService>;

  beforeEach(() => {
    fakeClockService = { clock$: Observable.timer(0, 500).pipe(map(val => val / 2)) }
    fakeColorsService = jasmine.createSpyObj('ColorsService', ['shuffleColors']);
    service = new BinService(
      timeThreshold,
      fakeClockService as GameClockService,
      fakeColorsService as ColorsService
    );
  });

  afterEach(() => {
    fakeClockService = null;
    fakeColorsService = null;
    service = null;
  })

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should emits time every second', fakeAsync(() => {
    let time = timeThreshold;
    const subscription = service.remainingTime$.subscribe(value => time = value);

    //start the timer
    tick(0);
    expect(time).toBe(timeThreshold);

    tick(500);
    expect(time).toBe(timeThreshold);

    // one second completed: must reduce the remaining time
    tick(500);
    expect(time).toBe(timeThreshold - 1);

    tick(500);
    expect(time).toBe(timeThreshold - 1);

    // another second completed: must reduce the remaining time
    tick(500);
    expect(time).toBe(timeThreshold - 2);

    subscription.unsubscribe();
  }));

  it('should restart the clock when values goes to zero', fakeAsync(() => {
    let time = timeThreshold;
    const subscription = service.remainingTime$.subscribe(value => time = value);

    //start the timer
    tick(0);
    expect(time).toBe(timeThreshold);

    // one second completed: must reduce the remaining time
    tick(1000);
    expect(time).toBe(timeThreshold - 1);

    // another second completed: must reduce the remaining time
    tick(1000);
    expect(time).toBe(timeThreshold - 2);

    // another second completed: must reduce the remaining time
    tick(1000);
    expect(time).toBe(timeThreshold - 3);

    // another second completed: must reduce the remaining time
    tick(1000);
    expect(time).toBe(timeThreshold - 4);

    // another second completed: must reduce the remaining time
    tick(1000);
    expect(time).toBe(timeThreshold);

    subscription.unsubscribe();
  }));

  it('colors should be drawn randomly on clock start/restart', fakeAsync(() => {
    let colors = [];
    const stubValues = [['red', 'blue'], ['green', 'orange']];

    fakeColorsService.shuffleColors.and.returnValues(...stubValues);
    const subscription = service.colors$.subscribe(value => colors = value);

    //start the timer
    tick(0);
    expect(colors).toBe(stubValues[0]);

    // one second completed: must reduce the remaining time
    tick(1000);
    expect(colors).toBe(stubValues[0]);

    // another second completed: must reduce the remaining time
    tick(1000);
    expect(colors).toBe(stubValues[0]);

    // another second completed: must reduce the remaining time
    tick(1000);
    expect(colors).toBe(stubValues[0]);

    // another second completed: must reduce the remaining time
    tick(1000);
    expect(colors).toBe(stubValues[0]);

    // cycle completed: clock restared
    tick(1000);
    expect(colors).toBe(stubValues[1]);

    subscription.unsubscribe();

    expect(fakeColorsService.shuffleColors).toHaveBeenCalledTimes(2);
  }));

});
