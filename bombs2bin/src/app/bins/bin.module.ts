import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BinService } from './bin.service';
import { BinDroppableComponent } from './bin-droppable/bin-droppable.component';
import { BinClockComponent } from './bin-clock/bin-clock.component';
import { BinAreaComponent } from './bin-area/bin-area.component';
import { GameClockService } from '../shared/game-clock.service';
import { ColorsService } from '../shared/colors.service';
import { SharedModule } from '../shared/shared.module';

export function BinServiceFactory(gameClockService: GameClockService, colorsService: ColorsService) {
  return new BinService(40, gameClockService, colorsService)
}

@NgModule({
  imports: [
    CommonModule,
    SharedModule
  ],
  declarations: [
    BinAreaComponent,
    BinClockComponent,
    BinDroppableComponent
  ],
  exports: [
    BinAreaComponent
  ],
  providers: [
    { 
      provide: BinService,
      deps: [GameClockService, ColorsService],
      useFactory: BinServiceFactory
    }
  ]
})
export class BinModule { }
