## Bombs 2 bin

### Scrapinghub Frontend Developer Trial

This repository contains the code for the short project proposed by Scrapinghub.


## Prerequisites 

The project was developed using the @angular-cli, so it has dependencies that require Node 6.9.0 or higher, together with NPM 3 or higher.

After installing the Node and NPM, you must install the angular-cli.

```bash

npm install -g @angular/cli

```

## Table of Contents

* [Demo](#demo)
* [Running it locally](#running-it-locally)
    * [Clone the project](#clone-the-project)
    * [Access the game folder](#access-the-game-folder)
    * [Install the dependencies](#install-the-dependencies)
    * [Run the code as production server](#production-server)
    * [Run the code as development server](#development-server)
    * [Notes](#notes)
* [License](#license)

## Demo 

If you want to play (*or test*) the game, you can access it on [http://thlima.com/scrapinghub/](http://thlima.com/scrapinghub/)

## Running it locally 

If you prefer to test it on your local machine, follow these steps:

**BEFORE YOU START:** please read the [prerequisites](#prerequisites)

### Clone the project 

```

git clone https://thiagoaslima@bitbucket.org/thiagoaslima/scrapinghub.git

```

### Access the game folder 

```

cd scrapinghub
cd bombs2bin

```

### Install the dependencies 

```

npm install

```

### Run the code as production server 

```

ng serve --prod -o

```

### Run the code as development server 

```

ng serve -o

```

#### Notes 

Because some problems with npm, it's possible that some problems happen when trying to run the code as production server.

* **Error**: [Module @angular-devkit/buildoptimizer not found](https://github.com/angular/angular-cli/issues/10019)

```bash
Cannot find module '@angular-devkit/buildoptimizer'
```

* **Possible solution**: Declare the module as a development dependency and install it

```bash
npm install --save-dev @angular-devkit/buildoptimizer
```

Other errors, please access the [@angular/cli Github page](https://github.com/angular/angular-cli)

Other possibility is only run the code as [development server](#development-server) 


## License

MIT

---